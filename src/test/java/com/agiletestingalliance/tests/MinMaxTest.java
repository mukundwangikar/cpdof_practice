package com.agiletestingalliance.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.agiletestingalliance.MinMax;

public class MinMaxTest {

	@Test
	public void testMin() {
		MinMax objMinMax = new MinMax();
		int result = objMinMax.f(10, 20);
		assertEquals(result, 20);
	}
	
	@Test
	public void testMax() {
		MinMax objMinMax = new MinMax();
		int result = objMinMax.f(30, 15);
		assertEquals(result, 30);
	}
}
