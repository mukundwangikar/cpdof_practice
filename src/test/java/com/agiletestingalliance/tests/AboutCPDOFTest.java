package com.agiletestingalliance.tests;

import org.junit.Test;

import com.agiletestingalliance.AboutCPDOF;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AboutCPDOFTest extends TestCase
{
    @Test
    public void testAboutCPDOF() {
    	AboutCPDOF objAboutCPDOF = new AboutCPDOF();
    	String actualString = "CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.";
    	assertEquals(objAboutCPDOF.desc(), actualString);
    }
}
